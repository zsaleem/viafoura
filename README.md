## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - HTML5.
 - Sass
 - jQuery
 - requirejs
 - Underscorejs(for minimal templates)
 - Git
 - Gitlab
 - [Bower](http://bower.io/).
 - Expressjs.
 - NPM
 - Vim.
 - Mac OS X.
 - Google Chrome Incognito
 - Grunt (as build tool) with following plugin!
   - [CSSmin](https://github.com/gruntjs/grunt-contrib-cssmin)
   - [Concat](https://github.com/gruntjs/grunt-contrib-concat)
   - [JSHint](https://github.com/gruntjs/grunt-contrib-jshint)
   - [Clean](https://github.com/gruntjs/grunt-contrib-clean)
   - [HTMLmin](https://github.com/gruntjs/grunt-contrib-htmlmin)
   - [ProcessHTML](https://github.com/dciccale/grunt-processhtml)
   - [Copy](https://github.com/gruntjs/grunt-contrib-copy)
   - [Nodemon](https://github.com/ChrisWren/grunt-nodemon)
   - [Concurrent](https://github.com/sindresorhus/grunt-concurrent)
   - [Watch](https://github.com/gruntjs/grunt-contrib-watch)
   - [Requirejs](https://github.com/gruntjs/grunt-contrib-requirejs)

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository. Make sure Git is installed on your systems.
    2. CD to the root folder of this project.
    3. Run `npm install` (if it complains then run `sudo npm install`) to install all Grunt plugins(dependencies). Please make sure that nodejs is installed on your machine before running npm install.
    4. Run `bower install` to install all project dependencies i.e. jquery into js/libs folder. Please make sure that bower is install on your machine before running bower install command.
    5. Now run `grunt` command on your terminal in `root` folder of this project.
    6. Now go to your browser and type `localhost:8000` to view this project in action.

## Description
Above steps, in getting started section, will install all dependencies required for this project to run and make the project ready for
production by minifying all resources. It will place the production ready project in `dist` folder in `root` level.

In this task, I followed MV* pattern. I created 2 folders inside `src/js/` i.e. `models` and `views`. Inside `models` I wrote `DialogBoxModel` which is responsible for getting data from Mock API(inside `src/dataset/dialog.json`) endpoint. And I filter data to make it appropriate for dialog box. I created the mock up json data for dialog from the design I received via Google Drive. Furthermore, In modal I am using jquery promises and I am calling one ajax request to get data from mock json data.

In `views` folder, I wrote `DialogBoxView.js` module. This view gets data from the modal, requires dialog box template inside `src/templates` folder and pass the mock data (received from model) to template where I add all the data to HTML. For templates, I am using minimal `underscorejs` template engine. `src/templates/` folder consists of template i.e. `DialogBox.html` as I mentioned earlier.

I wrote all CSS(Sass) in `src/styles/sass` folder. I created 2 folders inside `src/styles/sass` folder i.e. `base` contains global styles and `modules` contains `block` which each individual block on main page with image, title etc. `btn.scss` for `more` icon and `dialog.scss` has all the styles for dialog box.

I tested this task on Google Chrome Incognito and mac book air with a resolution of 1440x900.

## Notes
I got avatars from following link which are freely available with Free License

http://www.freepik.com/free-vector/people-avatars_761436.htm

I got majority of the text from Lorem Ipsum site which is following

http://www.lipsum.com/feed/html

The main 4 images are from png image I received via email for this task. I simply extracted them from the final.png image via photoshop.

