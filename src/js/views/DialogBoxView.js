define(function(require) {

    'use strict';

    var $ = require('jquery'),
        _ = require('underscore'),
        DialogTemplate = require('text!../../templates/DialogBox.html');

    var DialogBoxView = function(model, els) {
        this._model = model;
        this.$els = els;

        $('.more').on('click', this.render.bind(this));
        $('.overlay').on('click', this.closeDialog.bind(this));
    };

    DialogBoxView.prototype = (function() {

        var template = _.template(DialogTemplate);

        var render = function(e) {
            var id = $(e.target).data('id');

            this._model.get(id).then(function(res) {
                console.log(res[0]);
                $('.box').fadeIn();
                $('body').fadeIn().append(template({ res: res[0] }));
                $('.m-dialog').fadeIn();
            });

            e.preventDefault();
        },

        closeDialog = function() {
            $('.overlay').fadeOut();
            $('.m-dialog').fadeOut();
        };

        return {
            render: render,
            closeDialog: closeDialog
        }

    })();

    return DialogBoxView;
});

