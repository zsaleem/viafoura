define(function(require) {

    'use strict';

    var $ = require('jquery');

    var DialogBoxModel = function() {};

    DialogBoxModel.prototype = (function() {

        var _id = null;

        var get = function(id) {
            _id = id;
            return getReplies().then(filterReplies);
        },

        filterReplies = function(res) {
            return res.filter(function(reply) {
                if (_id == reply.discussionId) {
                    return reply.replies;
                }
            });
        },

        getReplies = function() {
            return $.ajax({
                url: '../dataset/dialog.json',
                method: 'GET',
                type: 'JSON'
            });
        };

        return {
            get: get
        }

    })();

    return DialogBoxModel;

});

