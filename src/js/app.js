/*global define:false */
define(function(require) {

	'use strict';

    var $ = require('jquery');

    var DialogBoxModel = require('models/DialogBoxModel'),
        DialogBoxView= require('views/DialogBoxView');

    var App = (function () {

        var init = function () {
            var dialogBoxModel = new DialogBoxModel(),
                dialogBoxView = new DialogBoxView(dialogBoxModel, {
                });
        };

        return {
            init: init
        };

    }());

	return App;
});
